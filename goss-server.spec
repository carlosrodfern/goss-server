# https://github.com/goss-org/goss
%global goipath         github.com/goss-org/goss
Version:                0.4.6
%global tag             v0.4.6

%gometa -f

%global goname goss-server

%global common_description %{expand:
Quick and Easy server testing/validation.}

%global golicenses      LICENSE
%global godocs          README.md

Name:           %{goname}
Release:        1%{?dist}
Summary:        Quick and Easy server testing/validation

License:        Apache-2.0
URL:            %{gourl}
Source0:        %{gosource}
Source1:        %{goname}.service
Source2:        %{goname}.conf
Source3:        %{goname}.yaml

BuildRequires:  systemd-rpm-macros

%description %{common_description}

%gopkg

%prep
%goprep -k
%autopatch -p1

%build
export GO111MODULE=on
%gobuild -o %{gobuilddir}/bin/goss %{goipath}/cmd/goss


%install
install -m 0755 -vd                     %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/
install -m 0644 -vpD %{S:1} %{buildroot}%{_unitdir}/%{goname}.service
install -m 0644 -vpD %{S:2} %{buildroot}%{_sysconfdir}/default/%{goname}
install -m 0644 -vpD %{S:3} %{buildroot}%{_sysconfdir}/%{goname}/%{goname}.yaml
# build man page

%pre

%post
%systemd_post %{goname}.service

%preun
%systemd_preun %{goname}.service

%postun
%systemd_postun_with_restart %{goname}.service

%files
%license LICENSE
%doc README.md
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/default/%{goname}
%dir %{_sysconfdir}/%{goname}/
%config(noreplace) %{_sysconfdir}/%{goname}/%{goname}.yaml
%{_unitdir}/%{goname}.service

%changelog
%autochangelog
