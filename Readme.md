# How to build locally

```sh
spectool -g goss-server.spec
rpkg srpm --outdir .
mock -r centos-stream-9-x86_64 --init
mock --enable-network -r centos-stream-9-x86_64 *.src.rpm
```

# How to test

```sh
cp /var/lib/mock/centos-stream-9-x86_64/result/goss-server-0.4.6-1.el9.x86_64.rpm ./tests/data/goss-server.rpm
tmt -vvv run
```
