#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "dnf install -y ./data/goss-server.rpm"
        rlServiceStart "goss-server.service"
    rlPhaseEnd

    rlPhaseStartTest
        rlWaitForCmd "curl -s http://localhost:8080/healthz" -t 60
        rlRun -s "curl -s -w '%{http_code}' http://localhost:8080/healthz"
        rlAssertGrep "200" $rlRun_LOG
    rlPhaseEnd

    rlPhaseStartCleanup
        rlServiceStop "goss-server.service"
        rlRun "dnf remove -y goss-server"
    rlPhaseEnd
rlJournalEnd
